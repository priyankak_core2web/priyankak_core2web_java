


class DIV{
	public static void main(String[]args){
		int x=28;
		if(x%3==0&&x%7==0){
			System.out.println(x + " is divisible by both 3&7");
		}else if(x%7==0){
			System.out.println(x + " is divisible by 7");
		}else if(x%3==0){
			System.out.println(x + " is divisible by 3 ");
		}else{
			System.out.println(x + " is neither divisible by 3 or 7");
		}
	}
}

