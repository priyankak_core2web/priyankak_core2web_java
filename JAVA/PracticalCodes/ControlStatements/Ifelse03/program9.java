

class Divisible{
	public static void main(String[]args){
		int Data=46;
		if(Data%2==0&&Data%5==0&&Data%10==0){
			System.out.println(Data + " is divisible by 2,5,10");
		}else if(Data%5==0){
			System.out.println(Data + " is divisible by 5");
		}else if(Data%2==0){
			System.out.println(Data + " is divisible by 2");
		}else{
			System.out.println(Data + " is not divisible by 2,5,10");
		}
	}
}

