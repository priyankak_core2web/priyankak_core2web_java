

import java.util.*;
class array5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrSize:");
		int size=sc.nextInt();
		int arr5[]=new int[size];
		for(int i=0;i<arr5.length;i++){
			arr5[i]=sc.nextInt();
		}
		int sum=0;
		for(int i=0;i<arr5.length;i++){
			if(i%2==1){
				sum=sum+arr5[i];
			}
		}
		System.out.println("Sum of odd indexed elements:"+sum);
	}
}


