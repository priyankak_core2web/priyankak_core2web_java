

import java.util.*;
class array10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrSize:");
		int size=sc.nextInt();
		int arr10[]=new int[size];
		for(int i=0;i<arr10.length;i++){
			arr10[i]=sc.nextInt();
		}
		int var=0;
		int max=arr10[0];
		for(int i=0;i<arr10.length;i++){
			if(arr10[i]>max){
				max=arr10[i];
				var=i;
			}
		}
		System.out.println("Maximum number found at position "+var+" is "+max);
	}
}


