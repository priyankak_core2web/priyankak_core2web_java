

import java.util.*;
class array6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrSize:");
		int size=sc.nextInt();
		int arr6[]=new int[size];
		for(int i=0;i<arr6.length;i++){
			arr6[i]=sc.nextInt();
		}
		int prod=1;
		for(int i=0;i<arr6.length;i++){
			if(i%2==1){
				prod=prod*arr6[i];
			}
		}
		System.out.println("Product of odd indexed elements:"+prod);
	}
}


