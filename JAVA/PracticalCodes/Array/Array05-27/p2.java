

import java.util.*;
class Array2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr2[]=new int[size];
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}
		int sum1=0;
		int sum2=0;
		for(int i=0;i<arr2.length;i++){
			if(arr2[i]%2==0){
				sum1=sum1+arr2[i];
			}else{
				sum2=sum2+arr2[i];
			}
		}
		System.out.println("Even sum:"+sum1);
		System.out.println("Odd sum:"+sum2);
	}
}


