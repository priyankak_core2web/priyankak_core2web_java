

import java.util.*;
class Array8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr8[]=new int[size];
		for(int i=0;i<arr8.length;i++){
			arr8[i]=sc.nextInt();
		}
		int min1=arr8[0];
		for(int i=0;i<arr8.length;i++){
			if(min1>arr8[i]){
				min1=arr8[i];
			}
		}
		int min2=arr8[0];
		for(int i=0;i<arr8.length;i++){
			if(min2>arr8[i]&&min1<arr8[i]){
				min2=arr8[i];
			}
		}
		System.out.println("The second minimum element in the array is: "+ min2);
	}
}


