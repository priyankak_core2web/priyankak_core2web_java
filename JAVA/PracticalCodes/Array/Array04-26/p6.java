
import java.util.*;
class Array6{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		char arr6[]=new char[size];
		for(int i=0;i<arr6.length;i++){
			arr6[i]=sc.next().charAt(0);
		}
		int countv=0;
		int countc=0;
		for(int i=0;i<arr6.length;i++){
			if(arr6[i]=='A'||arr6[i]=='E'||arr6[i]=='I'||arr6[i]=='O'||arr6[i]=='U'||arr6[i]=='a'||arr6[i]=='e'||arr6[i]=='i'||arr6[i]=='o'||arr6[i]=='u'){
				countv++;
			}else{
				countc++;
			}
		}
		System.out.println("Count of vowels:"+countv);
		System.out.println("Count of consonants:"+countc);
	}
}


