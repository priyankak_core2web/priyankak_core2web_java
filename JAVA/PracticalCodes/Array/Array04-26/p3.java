

import java.util.*;
class Array3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr3[]=new int[size];
		for(int i=0;i<arr3.length;i++){
			arr3[i]=sc.nextInt();
		}
		int max1=arr3[0];
		for(int i=1;i<arr3.length;i++){
			if(max1<arr3[i]){
				max1=arr3[i];
			}
		}
		int max2=arr3[0];
		for(int i=1;i<arr3.length;i++){
			if(max2<arr3[i]&&arr3[i]<max1){
				max2=arr3[i];
			}
		}
		System.out.println("The second largest element in the array is "+max2);
	}
}


