

import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr4[]=new int[size];
		for(int i=0;i<arr4.length;i++){
			arr4[i]=sc.nextInt();
		}
		System.out.print("Enter the check element:");
		int num=sc.nextInt();
		int count=0;
		for(int i=0;i<arr4.length;i++){
			if(num==arr4[i]){
				count++;
			}
		}
		if(count>2){
			System.out.println(num+" is occurs more than 2 times in an array");
		}else if(count<2){
			System.out.println(num+" is occurs less than 2 times in an array");
		}else{
			System.out.println(num+" is occurs 2 times in an array");
		}
	}
}

