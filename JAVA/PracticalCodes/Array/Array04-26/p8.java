

import java.util.*;
class Array8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		char arr8[]=new char[size];
		for(int i=0;i<arr8.length;i++){
			arr8[i]=sc.next().charAt(0);
		}
		System.out.print("Enter the character to check:");
		char ch=sc.next().charAt(0);
		int count=0;
		for(int i=0;i<arr8.length;i++){
			if(ch==arr8[i]){
				count++;
			}
		}
		System.out.println(ch+" occurs " + count+" times in the given array");
	}
}


