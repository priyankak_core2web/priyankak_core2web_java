

import java.util.*;
class Array2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr2[]=new int[size];
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}
		int min=arr2[0];
		for(int i=1;i<arr2.length;i++){
			if(min>arr2[i]){
				min=arr2[0];
			}
		}
		int max=arr2[0];
		for(int i=1;i<arr2.length;i++){
			if(max<arr2[i]){
				max=arr2[i];
			}
		}
		int diff=max-min;
		System.out.println("The difference between minimum and maximum element is "+ diff);
	}
}


