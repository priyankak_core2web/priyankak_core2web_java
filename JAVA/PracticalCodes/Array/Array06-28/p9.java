
import java.util.*;
class Palindrome{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr9[]=new int[size];
		for(int i=0;i<arr9.length;i++){
			arr9[i]=sc.nextInt();
		}
		int count=0;
		for(int i=0;i<arr9.length;i++){
			int temp=arr9[i];
			int num=temp;
			int rem=0;
			//int count=0;
			int rev=0;
			while(num>0){
				rem=num%10;
				rev=rem+rev*10;
				num/=10;
			}
			if(rev==temp){
				count++;
			}
		}
		System.out.println("Count of palindrome elements is:"+count);
	}
}

