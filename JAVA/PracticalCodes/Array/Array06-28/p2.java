
import java.util.*;
class Prime{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter arrsize:");
		int size=sc.nextInt();
		int arr2[]=new int[size];
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}
		int count=0;
		
		int sum=0;
		for(int i=0;i<arr2.length;i++){
			int temp=arr2[i];
			for(int j=temp/2;j>1;j--){
				if(temp%j==0){
					break;
				}
				if(j==2){
					count++;
					sum=sum+temp;
				}
			}
		}
		System.out.println("Sum of all prime numbers is "+ sum +" and count of prime numbers in the given array is "+ count);
	}
}



