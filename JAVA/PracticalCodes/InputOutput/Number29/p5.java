

import java.util.*;
class Automorphic{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num:");
		int num=sc.nextInt();
		int temp=num;
		int var=0;
		int rev=0;
		int digit=0;
		while(num>0){
			num/=10;
			digit++;
		}
		int sq=temp*temp;
		while(digit>0){
			int rem=sq%10;
			var=rem+var*10;
			sq/=10;
			digit--;
		}
		while(var>0){
			int arr=var%10;
			rev=arr+rev*10;
			var/=10;
		}
		if(rev==temp){
			System.out.println(temp+" is automorphic");
		}else{
			System.out.println(temp+" is not automorphic");
		}
	}
}


