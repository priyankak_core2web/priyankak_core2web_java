

import java.util.*;
class Strong{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num:");
		int num=sc.nextInt();
		int temp=num;
		int rem=0;
		int sum=0;
		int mult=1;
		while(num>0){
			rem=num%10;
			while(rem>0){
				mult=mult*rem;
				rem--;
				num/=10;
			}
			sum=sum+mult;
		}
		if(sum==temp){
			System.out.println(temp+" is Strong number");
		}else{
			System.out.println(temp+" is not Strong number");
		}
	}
}


