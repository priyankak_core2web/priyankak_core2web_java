

import java.util.*;
class Six{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			for(int space=1;space<=row-i;space++){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i*2-1;j++){
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}



