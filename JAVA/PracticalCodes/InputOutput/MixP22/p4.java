

import java.util.*;
class Four{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter row:");
		int row=sc.nextInt();
		int num=64;
		for(int i=1;i<=row;i++){
			int ch1=num+i;
			for(int space=1;space<i;space++){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=row-i+1;j++){
				if(row%2==1){
					System.out.print((char)ch1+" ");
				}else{
					System.out.print((char)(ch1+32)+" ");
				}
				ch1++;
			}
			System.out.println();
		}
	}
}


