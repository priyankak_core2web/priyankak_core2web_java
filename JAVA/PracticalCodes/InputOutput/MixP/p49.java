

import java.io.*;
class Character{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=row*(row+1)/2;
		int ch=num+64;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row-i+1;j++){
				System.out.print((char)ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}

