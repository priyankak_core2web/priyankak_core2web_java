

import java.io.*;
class Letter{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=0;
		int ch=row+64;
		for(int i=1;i<=row;i++){
			num=row+i-1;
			for(int j=1;j<=row;j++){
				System.out.print((char)ch+" ");
				System.out.print(num-- +" ");
			}

			System.out.println();
		}
	}
}


