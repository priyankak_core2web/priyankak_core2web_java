

import java.io.*;
class charSeq{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=0;
		int ch=0;
		for(int i=1;i<=row;i++){
			num=row;
			ch=row+96;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print((char)ch+" ");
					ch--;
				}else{
					System.out.print(num+" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}

