

import java.io.*;
class Character{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=row-i+65;
			for(int space=1;space<=row-i;space++){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i;j++){
				System.out.print((char)num+" ");
				num++;
			}
			System.out.println();
		}
	}
}

