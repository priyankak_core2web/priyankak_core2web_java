

import java.io.*;
class number{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=i;
			for(int space=1;space<i;space++){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=row-i+1;j++){
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}


