

import java.util.*;
class Eight{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int ch=row+64;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print("#"+" ");
					}else{
						System.out.print((char)ch+" ");
						ch--;
					}
				}else{
					if(j%2==0){
						System.out.print("#"+" ");
					}else{
						System.out.print((char)ch+" ");
						ch--;
					}
				}
			}
			System.out.println();
		}
	}
}


