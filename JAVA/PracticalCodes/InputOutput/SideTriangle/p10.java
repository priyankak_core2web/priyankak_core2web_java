

import java.util.*;
class CharReverse{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter row:");
		int row=sc.nextInt();
		int num=0;
		int col=0;
		for(int i=1;i<=row*2-1;i++){
			if(i<=row){
				col=row-i;
				num=col+65;
			}else{
				col=i-row;
				num=col+65;
			}
			for(int sp=1;sp<=col;sp++){
				System.out.print(" "+" ");
			}
			if(i<=row){
				col=i;
			}else{
				col=row*2-i;
			}
			for(int j=1;j<=col;j++){
				System.out.print((char)num+" ");
				num++;
			}
			System.out.println();
		}
	}
}




