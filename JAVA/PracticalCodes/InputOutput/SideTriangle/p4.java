

import java.util.*;
class Reverse{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter row:");
		int row=sc.nextInt();
		int num=row;
		int col=0;
		for(int i=1;i<=row*2-1;i++){
			if(i<=row){
				col=i;
			}else{
				col=row*2-i;
				num+=2;
			}
			for(int j=1;j<=col;j++){
				System.out.print(num+" ");
			}
			num--;
			System.out.println();
		}
	}
}



