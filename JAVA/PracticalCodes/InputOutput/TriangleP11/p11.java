

import java.io.*;
class Char{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		System.out.print("Enter ch:");
		int ch=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			ch=i+67;
			for(int j=1;j<=row-i+1;j++){
				System.out.print((char)ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}


