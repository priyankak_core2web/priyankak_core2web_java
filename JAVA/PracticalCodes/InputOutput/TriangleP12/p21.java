

import java.io.*;
class small{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=1;
		int ch=97;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(num+" ");
				}else{
					System.out.print((char)ch+" ");
					ch++;
				}
			}
			System.out.println();
			num++;
		}
	}
}


