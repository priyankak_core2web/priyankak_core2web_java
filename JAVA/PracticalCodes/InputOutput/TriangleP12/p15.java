

import java.io.*;
class Char{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=0;
		for(int i=1;i<=row;i++){
			num=97;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print((char)num+" ");
				}else{
					System.out.print("$"+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}



