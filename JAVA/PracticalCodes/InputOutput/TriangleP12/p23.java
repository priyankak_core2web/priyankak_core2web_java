

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int ch=97;
		for(int i=1;i<=row;i++){
			int num=row+1;
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(num+" ");
					num+=2;
				}else{
					System.out.print((char)ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}







