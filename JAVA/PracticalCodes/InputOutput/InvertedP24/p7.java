

import java.util.*;
class Seven{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int ch=65;
			for(int sp=1;sp<i;sp++){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=(row-i)*2+1;j++){
				if(j<row-i+1){
					System.out.print((char)ch+" ");
					ch++;
				}else{
					System.out.print((char)ch+" ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}

