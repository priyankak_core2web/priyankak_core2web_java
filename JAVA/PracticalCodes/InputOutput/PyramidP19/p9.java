

import java.io.*;
class Capital{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int ch1=0;
		int ch2=0;
		for(int i=1;i<=row;i++){
			int num=65;
			for(int space=row;space>i;space--){
				System.out.print(" "+"\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
					if(j<i){
						System.out.print((char)num+"\t");
						num++;
					}else{
						System.out.print((char)num+"\t");
						num--;
					}
				}else{
					if(j<i){
						System.out.print((char)(num+32)+"\t");
						num++;
					}else{
						System.out.print((char)(num+32)+"\t");
						num--;
					}
				}		
			}
			System.out.println();
		}
	}
}



