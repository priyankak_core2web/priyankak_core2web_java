

import java.io.*;
class numChar{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=1;
		for(int i=1;i<=row;i++){
			for(int space=row;space>i;space--){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
					System.out.print(num+" ");
				}else{
					System.out.print((char)(num+64)+" ");
				}
			}
			num++;
			System.out.println();
		}
	}
}


