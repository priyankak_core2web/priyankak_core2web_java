

import java.io.*;
class character{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=65;
		for(int i=1;i<=row;i++){
			for(int space=row;space>i;space--){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i*2-1;j++){
				System.out.print((char)num+" ");
			}
			num++;
			System.out.println();
		}
	}
}


