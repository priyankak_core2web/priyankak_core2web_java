

import java.io.*;
class number{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=1;
			for(int space=row;space>i;space--){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
					System.out.print(num++ +" ");
				}else{
					System.out.print(num-- +" ");
				}
			}
			System.out.println();
		}
	}
}


