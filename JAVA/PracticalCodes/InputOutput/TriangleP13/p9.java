


import java.io.*;
class Odd{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=(row*row)+3;
		int ch=(row*row)+4;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row-i+1;j++){
				if(row%2==0){
					System.out.print(num+" ");
					num-=2;
				}else{
					System.out.print(ch+" ");
					ch-=2;
				}
			}
			System.out.println();
		}
	}
}


