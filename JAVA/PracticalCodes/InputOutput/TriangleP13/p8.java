

import java.io.*;
class Rows{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		int num=0;
		int ch=0;
		for(int i=1;i<=row;i++){
			num=row-i+1;
			ch=num+64;
			for(int j=1;j<=row-i+1;j++){
				if(i%2==1){
					System.out.print(num+" ");
					num--;
				}else{
					System.out.print((char)ch+" ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}


